﻿#include <iostream>

using namespace std;

class Stack
{
public:
	int arr[10] = { 0,1,2,3,4,5,6,7,8 };
	
	int getArr(int num)
	{
		return arr[num];
	}
	int pop()
	{
		int massiveLength = sizeof(arr) / sizeof(arr[0]);
		return arr[massiveLength - 1];
	}
	int push(int num)
	{
		arr[9] = num;
		return 0;
	}
};


int main() {
	Stack st;
	int num;
	cout << "Get array element by index: ";
	cin >> num;
	cout << "Array with index " << num << " has: " << st.getArr(num) << endl << "Last element in array has: " << st.pop() << endl;
	int newElement;
	cout << "Num to push: ";
	cin >> newElement;
	st.push(newElement);
	cout << "Now last element in array has: " << st.pop();


}