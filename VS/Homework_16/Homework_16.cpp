﻿#include <iostream>
#include <ctime>
#pragma warning(disable : 4996)
using namespace std;

const int N = 3;

int main() {
    const int num = 10;
    const int num2 = 10;
    int list[num][num2];        // Двумерный массив num x num2
    for (int i = 0; i < num; i++) {
        for (int j = 0; j < num2; j++) {
            list[i][j] = i + j;  // Заполнение массива (каждый элемент равен сумме его координат) 
        }
    }

    for (int i = 0; i < num; i++) {  // Вывод массива в консоль 
        for (int j = 0; j < num2; j++) { 
            cout << list[i][j] << " "; 
        } 
        cout << endl; 
    } 
    
    time_t t = time(0);
    tm Localtime;

    localtime_s(&Localtime, &t);

    const int timing =(&Localtime)->tm_mday % N;

    int summa = 0;
    for (int i = 0; i < num; i++) {
        summa += list[timing][i];  // Суммируем все элементы массива 
    }

    cout << endl << summa;  // Сумма
}