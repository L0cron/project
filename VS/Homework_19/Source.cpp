#include <iostream>
#include <string>
using namespace std;


//�������� ������ C++ � Visual Studio (���������� ����������):

//�������� ����� Animal � ��������� ������� Voice(), ������� ������� � ������� ������ � �������.
//���������� �� Animal ������� ��� ������(� ������� Dog, Cat � �.�.) � � ��� ����������� ����� Voice() ����� �������, ����� ��� ������� � ������ Dog ����� Voice() ���������� Woof!� �������.
//� ������� main �������� ������ ���������� ���� Animal � ��������� ���� ������ ��������� ��������� �������.
//����� ���������� ������ �� �������, ������� �� ������ �������� ������� ����� Voice().
//������������� ��� ������.������ ���������� ��������� �� ����� ������� - ����������� Animal.




class Animal
{
public:
	string text;
	int Voice()
	{
		cout << text << endl; 
		return 0;
	};
};

class Fox : public Animal
{
public:
	Fox() {
		text = "Snort!";
		Voice();
	}
};

class Wolf : public Animal
{
public:
	Wolf() {
		text = "Woof!";
		Voice();
	}
};

class Cat : public Animal
{
public:
	Cat() {
		text = "Meow!";
		Voice();
	}
};


int main() {
	Animal array[3] = {Fox(), Wolf(), Cat()};
	for (int i = 0; i < 3; i++) {
		array[i];
	}
}