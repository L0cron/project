﻿#include <iostream>
#include <string>

using namespace std;

int main() {
	string text;
	cout << "Text: ";
	getline(cin, text);

	int textLength = text.length();

	char firstChar = text.at(0);
	char lastChar = text.at(textLength-1);

	cout << "Text: " << text << endl << "Length: " << textLength << endl << "First char: " << firstChar << endl << "Last char: " << lastChar;
}